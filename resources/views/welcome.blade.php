@extends ('layout.master')

@section('body')
    <section class="post row">
        <h4>Recent Post</h4>
         @foreach($posts as $post)
            <div class="col-md-6 col-md-offset-3 recent-post">
                <article>
                    <p>{{$post->body}}</p>
                </article>
                <div class="info">
                    posted by {{$post->name}} on {{$post->created_at}}
                </div>
                @if (Auth::check())
                <a href="{{ route('deletepost', [ 'post_id'=>$post->id ]) }}">Delete</a>
                @endif
            </div>
           </section>
        @endforeach

<div class="col-md-6 col-md-offset-3 createpost">
    <form method="post" action="{{ route('create') }}">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" required>
        </div>
        <div class="form-group">
            <label for="message">message</label>
            <textarea rows=5 class="form-control" name="body" required></textarea>
        </div>
        <input type="hidden" value="{{ Session::token() }}" name="_token">
        <button class="btn btn-primary">post</button>
    </form>
</div>
@endsection