@extends ('layout.master')

@section('body')

@if(Session::has('message'))
<div class="message col-md-4 col-md-offset-4">
    {{Session::get('message')}}
</div>
@endif

<div class="col-md-6 col-md-offset-3">
    <form action="{{route('userlogin')}}" method="post" name="loginForm">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="email">Password</label>
            <input type="password" name="password" class="form-control" required>
        </div>
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <button class='btn btn-primary'>Login</button>
    </form>
</div>

@endsection