@extends ('layout.master')

@section('body')

<div class="col-md-6 col-md-offset-3">
    <form action="{{route('usersignup')}}" method="post" name="signupForm">
        <div class="form-group">
            <label for="email">Name</label>
            <input type="text" name="name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="email">Password</label>
            <input type="password" name="password" class="form-control" required>
        </div>
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        <button class='btn btn-primary'>Signup</button>
    </form>
</div>


@endsection