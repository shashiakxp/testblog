<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    
    use DatabaseMigrations;
    use DatabaseTransactions;
   
   
    /**
     * A basic functional test example.
     *
     * @return void
     */
 
    public function testDashboard()
    {
        $this->visit('/')
             ->see('createpost')     
             ->see('recent post')
             ->click('Login')
             ->seePageIs('/login');
    }  
   
    public function testLogin()
    {
        $user = factory(App\User::class)->create([
            'email' => 'cyrup@admin.com',
            'password' => bcrypt('admin123'),
            'name' => 'cyrup'
        ]);
        
        $this->seeInDatabase('users', ['email' => 'cyrup@admin.com']);
        
        $this->visit('/login')
             ->type('cyrup@admin.com','email')
             ->type('admin123','password')
             ->press('Login')
             ->seePageIs('/dashboard')
             ->see('Hello cyrup');
    }
    
    public function testLoginFail()
    {
      
        $this->visit('/login')
             ->type('cyrup@admin.com','email')
             ->type('admin','password')
             ->press('Login')
             ->seePageIs('/login')
             ->see('Invalid username or password');
    }

    public function testCreatePost()
    {
        $this->visit('/')
             ->type('cyrup','name')
             ->type('abc123','body')
             ->press('post')
             ->seeInDatabase('posts', ['name' => 'cyrup'])
             ->seeInDatabase('posts', ['body' => 'abc123']);                   
    }
    
    public function testDeletePost()
    {
        $this->visit('/')
             ->type('cyrup','name')
             ->type('abc123','body')
             ->press('post')
             ->seeInDatabase('posts', ['body' => 'abc123'])
             ->dontSeeInDatabase('posts', ['body' => '45']);
                   
    }
    
    public function testUserSignup()
    {
        $this->visit('/signup')
             ->type('44','name')
             ->type('44@gmail.com','email')
             ->type('44','password')
             ->press('Signup')
             ->seeInDatabase('users', ['email' => '44@gmail.com'])   
             ->seeInDatabase('users', ['name' => '44']);          
    }
    
    public function testUserLogout()
    {
         $user = factory(App\User::class)->create([
            'email' => 'cyrup@admin.com',
            'password' => bcrypt('admin123'),
            'name' => 'cyrup'
        ]);
        
         $this->visit('/login')
              ->type('cyrup@admin.com','email')
              ->type('admin123','password')
              ->press('Login')
              ->click('Logout')
              ->seePageIs('/dashboard');    
    }
    
}
