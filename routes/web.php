<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::post('/create', [
    'uses' => 'PostController@postCreatePost',
    'as'   => 'create'
]);

Route::get('/dashboard', [
    'uses' => 'PostController@getDashboard',
    'as'   => 'dashboard'
]);

Route::get('/login', [
    'uses' => 'UserController@getLogin',
    'as'   => 'login'
]);

Route::post('/userlogin', [
    'uses' => 'UserController@postUserLogin',
    'as'   => 'userlogin'
]);

Route::post('/usersignup', [
    'uses' => 'UserController@postSignup',
    'as'   => 'usersignup'
]);

Route::get('/signup', [
    'uses' => 'UserController@getSignup',
    'as'   => 'signup'
]);


Route::get('/userlogout', [
    'uses' => 'UserController@getUserLogout',
    'as'   => 'userlogout'
]);

Route::get('/delete/{post_id}', [
    'uses' => 'PostController@getDeletePost',
    'as'   => 'deletepost' 
]);

