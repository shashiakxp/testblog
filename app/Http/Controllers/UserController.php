<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getLogin()
    {
         return view('login');
    }

    public function getSignup()
    {
        return view('signup');
    }    
    
    public function postSignup(Request $request)
    {
        $name = $request['name'];
        $email = $request['email'];
        $password = bcrypt($request['password']);

        $user = new User();

        $user->email = $email;
        $user->password = $password;
        $user->name = $name;

        $user->save();

        return redirect()->back();       
    }    
    
    public function postUserLogin(Request $request)
    {         
        $message = 'Invalid username or password';
            
        if (Auth::attempt($request->only(['email','password']))) {
            return redirect('dashboard');
        } else {            
            return redirect()->back()->with(['message' => $message]);
        }
    }

    public function getUserLogout()
    {
        Auth::logout(); 
        return redirect()->route('dashboard');
    }
}