<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function postCreatePost(Request $request)
    {
        $post = new Post();
        $post->body = $request['body'];
        $post->name = $request['name'];
        $post->save();
        return redirect()->route('dashboard');        
    }
    
    public function getDashboard(Request $request)
    {
        $post = Post::all();
        return view('welcome', [ 'posts' => $post ]);
    }   
    
    public function getDeletePost($post_id)
    {
        $post = Post::where('id', $post_id)->first();
        $post->delete();
        return redirect()->route('dashboard');
    }
}
                      							                      

